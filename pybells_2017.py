"""
Python bells.
Old school Christmas song wave generator in pure Python (3) - 2017 Edition

Please don't take this code too seriously.... ;-)

Merry Christmas!
Lorenzo.
"""

import wave
import struct
import math
import itertools
import operator

# -------- GLOBAL CONSTANTS AND VALUES -------- #
SAMPLE_RATE = 44000         # sampe rate for the song
BIT_FORMAT = 2              # 1: 8 bit, 2: 16 bit
MAX_AMPLITUDE = (2 ** (BIT_FORMAT * 8) // 2) - 1
PACK_TYPE = {1: 'b', 2: 'h'}    # The type to use
MASTER_VOLUME = 1.0            # max volume
PI = math.pi
TWO_PI = 2 * PI
SQRT_12_2 = 1.0594630943592953 # 2 ** (1/12)
FREQ_FACTOR = 1         # frequency factor - 1.0 is same midi notes etc.
CHANS = 2              # Channels in wave file

# -------- SONG GLOBALS -------- #
# Single note duration in sample. This essentially defines the song 'tempo'
NOTE_DURATION = 8000
# Attack and decay factors for each note (as NOTE_DURATION ratio)
ATTACK_FACTOR = 0.018
DECAY_FACTOR = 0.2
PAUSE_NUMBER = -1       # Note number for a 'pause'

REPETITIONS = 1         # Repetitions of the song
WAVE_FILE = "python_bells_2017.wav"

# Helper functions
def mtof(distance):
    """ See http://en.wikipedia.org/wiki/Equal_temperament for details...
Midi note number to frequency in Hertz.
distance is the distance (positive or negative) from A440 Hz key i.e. 69
    """
    p_a = 440.0
    a_ref = 69
    return p_a * (math.pow(SQRT_12_2, (distance - a_ref)))

def cycle_calc(freq, sample_rate, amplitude_factor=1.0):
    """ Calculate cycle for  given frequency, optionally multiply by factor """
    return int(sample_rate / freq) / amplitude_factor

def make_ramp(min_i, max_i, size, amplitude):
    """ Make a linear ramp list of floats from begin to end of size """
    size = math.floor(size)
    interval_range = max_i - min_i
    single_step = (interval_range / size) * amplitude
    ramp = [min_i]
    for _ in range(1, size):
        ramp.append((ramp[_ - 1] + single_step))
    return ramp

def delay_delta(samples):
    """ Generate a list of zeros of length dureation (semples) """
    del_line = [0] * (int(samples))
    return del_line

def make_note(
        midi_num,
        note_duration,
        attack_list,
        decay_list,
        harmonic_list,
        amplitude=1,
        func=math.sin,
        pause_num=-1,
        frquency_factor=1.0,
        semple_rate=SAMPLE_RATE,
    ):
    """ Generate the list of samples for a single note (or a pause)
The func is the actual function for the wave. Default is sine.

harmonic_list is a list of tuples each representing overtone ratio and amplitude
for example: [(1.0, 1.0), (2.0, 0.5)] will contain the first overtone with
amplitude 1.0 and the second harmonic (overtone with ratio 2.0) with half ampl.
    """
    # This was useful: https://v.gd/sBGS14
    # Is this a pause. In such case we just return a list o zeros?
    if midi_num == pause_num:
        return [0] * note_duration

    sample_list = []
    note_freq = mtof(midi_num)

    # cycle_list contains the cycles for the note based on the harmonics
    cycle_list = []

    for harm, amp in harmonic_list:
        this_cycle = cycle_calc(note_freq * harm, semple_rate, frquency_factor)
        cycle_list.append((this_cycle, amp))

    decay_duration = len(decay_list)

    # Here we generate the actual samples
    for i in range(0, note_duration):
        # Are we within the attack samples? If so scale the volume accordingly
        if i < len(attack_list):
            volume = attack_list[i]

        # Similarly if we are within decay samples (at the end)...
        if note_duration - i <= decay_duration:
            volume = decay_list[note_duration - i - 1]

        # Finally calculate sample value and append to value list
        volume_const = amplitude * volume * (2 / len(cycle_list))

        value = 0
        for (cycle, amp) in cycle_list:
            this_sample = int(
                func((i % cycle) / cycle) * volume_const * amp
            )
            value += this_sample

        sample_list.append(value)
    return sample_list

def make_melody(note_list, sample_dictionary):
    """ Generate a list of samples from notes and the notes dictionary list """
    sample_list = []

    for note in note_list:
        sample_list.extend(sample_dictionary[note])
    return sample_list

# melody voices as midi note numbers. -1 is a 'pause' (silence)
MELODY_VOICE1 = [
    55,
    55, 64, 62, 60, 55, -1, -1, 55,     55, 64, 62, 60, 57, -1, -1, 57,
    57, 65, 64, 62, 59, -1, -1, 59,     67, 67, 65, 62, 64, -1, -1, 55,

    55, 64, 62, 60, 55, -1, -1, 55,     55, 64, 62, 60, 57, -1, -1, 57,
    57, 65, 64, 62, 67, 67, 67, 67,     69, 67, 65, 62, 60, -1, 67, -1,

    64, 64, 64, -1, 64, 64, 64, -1,     64, 67, 60, 62, 64, -1, -1, -1,
    65, 65, 65, 65, 65, 64, 64, 64,     64, 62, 62, 64, 62, -1, 67, -1,

    64, 64, 64, -1, 64, 64, 64, -1,     64, 67, 60, 62, 64, -1, -1, -1,
    65, 65, 65, 65, 65, 64, 64, 64,     67, 67, 65, 62, 60, -1, -1, -1,
    ]

MELODY_VOICE2 = [
    -1,
    48, -1, 43, -1, 48, -1, 43, -1,     48, -1, 43, -1, 41, -1, 48, -1,
    41, -1, 48, -1, 43, -1, 50, -1,     43, -1, 50, -1, 48, 43, 45, 47,

    48, -1, 43, -1, 48, -1, 43, -1,     48, -1, 43, -1, 41, -1, 48, -1,
    48, -1, 41, -1, 43, -1, 50, -1,     43, -1, 50, -1, 48, 47, 45, 43,

    48, -1, 43, -1, 48, -1, 43, -1,     48, -1, 41, -1, 48, 36, 38, 40,
    41, -1, 48, -1, 48, -1, 43, -1,     38, -1, 42, -1, 43, -1, 50, -1,

    48, -1, 43, -1, 48, -1, 43, -1,     48, -1, 41, -1, 48, 36, 38, 40,
    41, -1, 48, -1, 48, -1, 43, -1,     43, 43, 45, 47, 48, -1, -1, -1,
    ]

FINALE_VOICE1 = [
    67, -1, 68, -1, 69, -1, 71, -1,     72, -1, -1, -1, 76, -1, -1, -1,
    72, -1, -1, -1, 76, -1, -1, -1,     72, -1, 76, -1, 72, -1, 76, -1,

    72, -1, 79, -1, 72, -1, 76, -1,     72, 76, 72, 76, 72, 76, 72, 76,
    72, -1, 84, -1, 79, -1, 76, -1,     72, -1, 72, -1, 67, -1, 64, -1,

    60, -1, -1, -1, 72, -1, 72, -1,     72, -1, 72, -1, 72, -1, 76, -1,
    72, -1, -1, -1, 72, -1, -1, -1,     67, -1, -1, -1, 64, -1, -1, -1,

    60, -1, -1, 48, 48, -1, 48, -1,     36, -1, -1, -1, -1, -1, -1, -1,
]

FINALE_VOICE2 = [
    55, -1, 54, -1, 53, -1, 50, -1,     48, -1, -1, -1, 43, -1, -1, -1,
    48, -1, -1, -1, 43, -1, -1, -1,     48, -1, 43, -1, 48, -1, 43, -1,

    48, -1, 43, -1, 48, -1, 43, -1,     48, 43, 48, 43, 48, 43, 48, 43, 
    48, -1, 48, -1, 48, -1, 48, -1,     48, -1, 48, -1, 48, -1, 48, -1,

    48, -1, 43, -1, 48, -1, 43, -1,     48, 43, 48, 43, 48, 43, 48, 43, 
    48, -1, 48, -1, 48, -1, 48, -1,     48, -1, 48, -1, 48, -1, 48, -1,

    36, -1, -1, 36, 36, -1, 36, -1,     24, -1, -1, -1, -1, -1, -1, -1,
]

MELODY_VOICE1_OCT_UP = [x + 12 if x >= 0 else -1 for x in MELODY_VOICE1]
PARTS = [
    MELODY_VOICE1 + MELODY_VOICE1_OCT_UP + FINALE_VOICE1,
    MELODY_VOICE2 + MELODY_VOICE2 + FINALE_VOICE2,
]

if __name__ == "__main__":
    print("Calculating notes...")
    # precompute lists of attack and decay amplitudes for each sample
    attack_amps = make_ramp(
        0, MAX_AMPLITUDE, NOTE_DURATION * ATTACK_FACTOR, MASTER_VOLUME
        )
    decay_amps = make_ramp(
        0, MAX_AMPLITUDE, NOTE_DURATION * DECAY_FACTOR, MASTER_VOLUME)

    harmonics = [
        (0.5, 0.8),
        (0.25, 0.4),
        (1.0, 1.0),
        (1.01, 0.9),
        (2.015, 0.8),
        (4.0, 0.2),
        ]

    # pre-compute only samples for needed notes and store them in a dictionary
    # First we extract only the unique notes by converting the list to a set..
    unique_notes = set(itertools.chain.from_iterable(PARTS))
    note_dictionay = {}
    for this_note in unique_notes:
        note_dictionay[this_note] = make_note(
            this_note,
            NOTE_DURATION,
            attack_amps,
            decay_amps,
            harmonics,
            func=math.atan
        )

    print("Making the melodies...")
    melody_list = []

    for this_part in PARTS:
        this_melody = make_melody(this_part, note_dictionay)
        melody_list.append(this_melody)

    print("DJ at the mixer...")
    sample_sum = list(map(operator.add, melody_list[0], melody_list[1]))

    print("Delay... elay... lay... ay...")
    final_samples = []

    # Generate two delay lines to simulate an "echo" effect
    DELAY1 = delay_delta(NOTE_DURATION)
    DELAY2 = delay_delta(NOTE_DURATION * 1.5)
    DELAY1.extend([x * 0.028 for x in sample_sum])
    DELAY2.extend([x * 0.009 for x in sample_sum])

    print("Santa's Magic Stereo...")
    # Interleave for stereo...
    for index, sample in enumerate(sample_sum):
        # Echo-type effect with one delay line on each stereo channel
        c1 = sample  + DELAY1[index]
        c2 = sample  + DELAY2[index]
        final_samples.append(c1)
        final_samples.append(c2)

    print("Elves packing the data...")
    packed_values = map(
        lambda x: struct.pack(PACK_TYPE[BIT_FORMAT], int(x * 0.3)), final_samples
    )
    value_str = (b''.join(packed_values) * REPETITIONS)

    wave_output = wave.open(WAVE_FILE, 'w')
    wave_output.setparams((CHANS, BIT_FORMAT, SAMPLE_RATE, 0, 'NONE', None))
    wave_output.writeframes(value_str)
    wave_output.close()
    print("Your masterpiece is in {}".format(WAVE_FILE))
    art = """                *
               ***
              *****
             *******
            *********
               ***

-----==== Merry Christmas! ====----

    """
    print(art)
